#include <iostream>

using namespace std;
typedef int Elemtype;
//插入排序
//插入排序 ---- 直接插入排序
void InsertSort(Elemtype A[], int n)
{
    int i , j ;
    for(i = 2 ; i <= n ; i++){
        if(A[i] < A[i-1]){
            A[0] = A[i];
            for(j = i -1; A[j] > A[0] ;--j){
                A[j+1] = A[j];
            }
            A[j+1] = A[0];
        }
    }
}
//插入排序 ---- 折半插入排序
void InsertHarfSort(Elemtype A[], int n)
{
    int i , j ,mid , high , low;
    for(i = 2 ; i <= n ; i++){
        if(A[i] < A[i-1]){
            A[0] = A[i];
            low = 1;
            high = i-1;


            while (low <= high) {
                mid = (low + high) / 2;
                if(A[mid] > A[0])
                      high = mid - 1;
                else
                      low = mid + 1;
            }
            for(j = i - 1 ; j >= high + 1 ; j--){
                    A[j + 1] = A[j];
            }
            A[high + 1] = A[0];
        }
    }
}

//插入排序 ---- 希尔排序
//交换排序
void swap(int &a , int &b)
{
    int temp = a;
    a = b;
    b = temp;
}
//交换排序 ---- 冒泡排序
void bubbSort(Elemtype A[], int n)
{
    int i , j;
    bool flag = false;
    for(i = 0; i < n - 1 ; i++)
    {
        for(j = n - 1 ; j > i ; j--){
            if(A[j] < A[j - 1]){
                swap(A[j] , A[j - 1]);
                flag  = true;
           }
           if(flag == false)
                return;
        }
    }
}
//交换排序 ---- 快排
int pivotion(Elemtype A[], int low , int high)
{
    int pivot = A[low];
    while(low < high){
        while(low < high && A[high] > pivot) high--;
            A[low] = A[high];
        while(low < high && A[low] < pivot) low++;
            A[high] = A[low];
    }
    A[low] = pivot;
    return low;
}

void QuickSort(Elemtype A[] , int low , int high)
{
    if(low < high){
        int pivot = pivotion(A,low,high);
        QuickSort(A,low , pivot - 1);
        QuickSort(A, pivot + 1 , high);
    }

}


//选择排序

//选择排序 ---- 简单选择排序
void SelectSort(Elemtype A[] , int n)
{
    int min;
    int i , j ;
    for(int i = 0 ; i < n ; i++){
        min = i;
        for(j = i + 1; j < n ; j++){
            if(A[j] < A[min])
                min = j;
        }
        if(min != i)
            swap(A[i], A[min]);
    }
}

//选择排序 ---- 堆排序

void HeadAdjust(Elemtype A[] , int k , int len)
{
    int i;
    A[0] = A[k];
    cout << "[k]" << k << endl;

    for(i = 2*k; i <= len; i*= 2){
        cout << "for" << i << endl;
        if(i < len && A[i] < A[i+1]){
            i++;
        }

        if(A[0] >= A[i]) break;
        else {
            A[k] = A[i];
            k = i;
        }

    }//for
    A[k] = A[0];
    cout << "=================================" << endl;
}

void BuildHeap(Elemtype A[], int len)
{
    for(int i = len /2; i > 0 ; i--){
//        cout << " * " << i;
//        cout << endl;
        HeadAdjust(A,i,len);
    }
}

void HeapSort(Elemtype A[], int len)
{
    BuildHeap(A,len);
    for(int i = len; i > 1; i--){
        swap(A[i],A[1]);
        HeadAdjust(A,1,i-1);
    }
}
int main()
{
    Elemtype A[10];
    A[0] = 10 ;A[1] = 7;A[2] = 0;A[3] = 3;A[4] = 4;A[5] = 6;A[6] = 2;A[7] = 1;A[8] = 9;A[9] = 8;
    for(int i = 0 ; i < 10 ; i++){
        cout << A[i] << " ";
    }
    cout << "" << endl;
//  InsertHarfSort(A,9);
//  bubbSort(A,10);
//  QuickSort(A,0,9);
//  SelectSort(A,10);
    HeapSort(A,10);
    for(int i = 0 ; i < 10 ; i++){
        cout << A[i] << " ";
    }
    cout << "" << endl;
    return 0;
}
